package com.example.shubh.project_1;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class MyViewPagerAdapter extends PagerAdapter {
    Activity activity;
    ArrayList<Integer> images;
    LayoutInflater inflater;
    public MyViewPagerAdapter(FragmentActivity activity, ArrayList<Integer> picArray) {
        this.activity = activity;
        this.images = picArray;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
        //super.destroyItem(container, position, object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View v = inflater.inflate(R.layout.viewpager_sliding_imageview,container,false);

        ImageView myImage = v.findViewById(R.id.imageView);
        myImage.setImageResource(images.get(position));

        container.addView(v,0);

        return v;
        //return super.instantiateItem(container, position);
    }


}
