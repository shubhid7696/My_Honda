package com.example.shubh.project_1.Extra_Classes;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shubh.project_1.R;
import com.example.shubh.project_1.User_Fragments.ProfileFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class FirebaseMethods {


    private Map newPost = new HashMap();
    private String user_id;
    private Dialog dialog;
    private Activity activity;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference,currentUserDBRefrence;
    Common_Methods commonMethods;

    public FirebaseMethods(Activity activity) {
        this.activity = activity;
        firebaseAuth = FirebaseAuth.getInstance();
        Common_Methods.USER_ID = firebaseAuth.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        dialog = new Dialog(activity);
    }



    public void setBookingDataOnFirebase(Map newPost) {
        this.newPost = newPost;
        String abc = Common_Methods.USER_ID;
        currentUserDBRefrence = databaseReference.child("bookings").child(abc);
        currentUserDBRefrence.setValue(newPost);

    }

    public void setTsestDataOnFirebase(Map newPost) {
        this.newPost = newPost;
        String abc = Common_Methods.USER_ID;
        currentUserDBRefrence = databaseReference.child("testing").child(abc);
        currentUserDBRefrence.setValue(newPost);
    }

    public void checkLogin() {
        new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Common_Methods.currentUser = firebaseAuth.getCurrentUser();
                    Common_Methods.USER_ID = user.getUid();
                } else {
                    // No user is signed in
                    Common_Methods.currentUser = null;
                    Common_Methods.USER_ID = null;
                }

            }
        };
    }


    public void signInToFirebaseWithEmailPassword(String remail, String rpassword, final FragmentActivity activity) {
        final ProgressDialog pd = new ProgressDialog(activity);
        pd.setTitle("Logging in");
        pd.setMessage("Please wait...");
        firebaseAuth.createUserWithEmailAndPassword(remail,rpassword).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new ProfileFragment()).commit();
                }
                else {
                    //.setError("Invalid Email or\nE_mail already exist");
                    commonMethods = new Common_Methods(activity);
                    commonMethods.showDialog("","Wrong Email Or Password \nor\nDuplicate Email");
                    //Toast.makeText(activity, "Registration failed\nPlease give valid Email...\nOR\nE_mail already exist", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
