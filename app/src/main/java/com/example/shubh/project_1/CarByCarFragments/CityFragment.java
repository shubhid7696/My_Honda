package com.example.shubh.project_1.CarByCarFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.shubh.project_1.MyViewPagerAdapter;
import com.example.shubh.project_1.R;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;


/**
 * A simple {@link Fragment} subclass.
 */
public class CityFragment extends Fragment implements View.OnClickListener {

    Button b1,b2,b3,b4,b5;
    ImageView iv;
    ViewPager vp;
    Integer city_pics[] = {R.drawable.ci1,R.drawable.ci2,R.drawable.ci3,R.drawable.ci4,R.drawable.ci5,R.drawable.ci6,
            R.drawable.ci7};

    ArrayList<Integer> picArray = new ArrayList<Integer>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_city, container, false);

        iv = v.findViewById(R.id.city_color);

        b1 = v.findViewById(R.id.bcity_red);
        b2 = v.findViewById(R.id.bcity_white);
        b3 = v.findViewById(R.id.bcity_silver);
        b4 = v.findViewById(R.id.bcity_brown);
        b5 = v.findViewById(R.id.bcity_silver_metalic);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);

        vp = v.findViewById(R.id.pager);
        for(int i=0; i<city_pics.length; i++) {
            picArray.add(city_pics[i]);
        }

        vp.setAdapter(new MyViewPagerAdapter(getActivity(),picArray));

        CircleIndicator indicator = v.findViewById(R.id.indicator);
        indicator.setViewPager(vp);

        return v;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bcity_red:
                iv.setImageResource(R.drawable.city_red);
                break;
            case R.id.bcity_white:
                iv.setImageResource(R.drawable.city_white);
                break;
            case R.id.bcity_silver:
                iv.setImageResource(R.drawable.city_brown);
                break;
            case R.id.bcity_brown:
                iv.setImageResource(R.drawable.city_master);
                break;
            case R.id.bcity_silver_metalic:
                iv.setImageResource(R.drawable.city_albastersilver);
                break;
        }
    }
}
