package com.example.shubh.project_1.CarByCarFragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.example.shubh.project_1.Activities.WebViewActivity;
import com.example.shubh.project_1.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class BRVFragment extends Fragment {

    WebView wv;
    TextView tv1,tv2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_brv, container, false);
        tv1 = v.findViewById(R.id.textView8);
        tv2 = v.findViewById(R.id.textView9);
        wv = v.findViewById(R.id.brv_webview);

        WebSettings webSettings = wv.getSettings();
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webSettings.setJavaScriptEnabled(true);
        wv.setWebViewClient(new WebViewClient());
        wv.loadUrl("https://www.hondacarindia.com/honda-brv");
        return v;
    }

    class MyWebViewClient extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
    }
}
