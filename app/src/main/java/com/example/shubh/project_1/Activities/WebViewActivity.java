package com.example.shubh.project_1.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebBackForwardList;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.example.shubh.project_1.R;
import com.github.clans.fab.FloatingActionMenu;

public class WebViewActivity extends AppCompatActivity {

    WebView webView;
    FloatingActionMenu fabMenu;
    com.github.clans.fab.FloatingActionButton fab,main_site;
    String website_url;
    boolean temp = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        Intent intent = getIntent();
        website_url = intent.getStringExtra("website_name");

        webView = findViewById(R.id.honda_site);
        fab = findViewById(R.id.fab_home);
        main_site = findViewById(R.id.main_site);
        fabMenu = findViewById(R.id.floating_action_menu);

        //enable cookies in webview of he application.
        CookieManager.getInstance().setAcceptCookie(true);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }else {
            CookieManager.getInstance().setAcceptCookie(true);
        }

        webViewMethod();

        //********** floating action button home **********
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabMenu.close(true);
                startActivity(new Intent(WebViewActivity.this,MainActivity.class));
                finish();
            }
        });
        main_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(temp){
                    fabMenu.close(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            main_site.setLabelText(getResources().getString(R.string.india_site));
                        }
                    }, 7500);
                    temp = false;
                    website_url = getResources().getString(R.string.main_site);
                    webView.loadUrl(website_url);
                }

                else{
                    fabMenu.close(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            main_site.setLabelText(getResources().getString(R.string.main_site));
                        }
                    }, 7500);
                    temp=true;
                    website_url = getResources().getString(R.string.india_site);
                    webView.loadUrl(website_url);
                }
            }
        });
    }

    private void webViewMethod() {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        //setting cache for the webview
        webView.getSettings().setAppCacheMaxSize( 10 * 1024 * 1024 ); //10MB
        webView.getSettings().setAppCachePath( getApplicationContext().getCacheDir().getAbsolutePath() );
        webView.getSettings().setAllowFileAccess( true );
        webView.getSettings().setAppCacheEnabled( true );
        webView.getSettings().setJavaScriptEnabled( true );
        webView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );
        //Setting cliewn url and goBack feature
        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl(website_url);//link to load page.
        webView.canGoBack();//add can go back feature.

    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack())
            webView.goBack();
        super.onBackPressed();
    }

    class MyWebViewClient extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
    }
}
