package com.example.shubh.project_1.CarByCarFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.shubh.project_1.MyViewPagerAdapter;
import com.example.shubh.project_1.R;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;


/**
 * A simple {@link Fragment} subclass.
 */
public class AmazeFragment extends Fragment {

    ViewPager vp,vp2;
    Integer amaze_pics[] = {R.drawable.amaze0,R.drawable.amaze1,R.drawable.amaze2,R.drawable.amaze3,R.drawable.amaze4,R.drawable.amaze5};
    Integer amaze_interior[] = {R.drawable.ami1,R.drawable.ami2,R.drawable.ami3,R.drawable.ami4};
    ArrayList<Integer> picArray1 = new ArrayList<Integer>();
    ArrayList<Integer> picAraay2 = new ArrayList<Integer>();
    CircleIndicator indicator1,indicator2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_amaze, container, false);
        vp = v.findViewById(R.id.pager1);
        vp2 = v.findViewById(R.id.pager2);

        for(int i=0; i<amaze_pics.length; i++) {
            picArray1.add(amaze_pics[i]);
        }

        for(int i=0;i<amaze_interior.length;i++){
            picAraay2.add(amaze_interior[i]);
        }

        vp.setAdapter(new MyViewPagerAdapter(getActivity(),picArray1));
        vp2.setAdapter(new MyViewPagerAdapter(getActivity(),picAraay2));

        indicator1 = v.findViewById(R.id.indicator1);
        indicator1.setViewPager(vp);
        indicator2 = v.findViewById(R.id.indicator2);
        indicator2.setViewPager(vp);

        return v;
    }

}
