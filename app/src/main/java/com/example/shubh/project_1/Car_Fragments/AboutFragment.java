package com.example.shubh.project_1.Car_Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.shubh.project_1.Activities.WebViewActivity;
import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.example.shubh.project_1.MyViewPagerAdapter;
import com.example.shubh.project_1.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {

    static final Integer[] images = {R.drawable.sos1,R.drawable.sos2,R.drawable.sos3};
    private ArrayList<Integer> picArray = new ArrayList<Integer>();
    ViewPager vp;
    TextView tv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_about, container, false);

        getActivity().setTitle("About");
        tv = v.findViewById(R.id.website_link);

        for(int i=0; i<images.length; i++) {
            picArray.add(images[i]);
        }
        vp = v.findViewById(R.id.about_pager);
        vp.setAdapter(new MyViewPagerAdapter(getActivity(),picArray));

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), WebViewActivity.class));
                getActivity().finish();
            }
        });
        return v;
    }

}
