package com.example.shubh.project_1.User_Fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.example.shubh.project_1.Extra_Classes.FirebaseMethods;
import com.example.shubh.project_1.R;
import com.example.shubh.project_1.Extra_Classes.Shared_Prefs;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;


/**
 * A simple {@link Fragment} subclass.
 */
public class LogIn extends Fragment{

    EditText u_name,passwd;
    TextView tv;
    String L_USERNAME,L_PASSWORD,sp_uname,sp_pwd;
    Button LOGIN;
    Shared_Prefs shared_prefs;
    FirebaseAuth firebaseAuth;
    SignInButton g_Button;
    private static final int RC_SIGN_IN = 3;
    GoogleSignInClient mGoogleSignInClient;
    CallbackManager mCallbackManager;
    FirebaseMethods firebaseMethods;
    LoginButton loginButton;
    Common_Methods commonMethods;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_log_in, container, false);

        getActivity().setTitle("Log-In");
        shared_prefs = new Shared_Prefs(getActivity());
        u_name = v.findViewById(R.id.username);
        passwd = v.findViewById(R.id.password);
        LOGIN = v.findViewById(R.id.Login_Button);
        tv = v.findViewById(R.id.register);
        g_Button = v.findViewById(R.id.g_login);


        commonMethods = new Common_Methods(getActivity());
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseMethods = new FirebaseMethods(getActivity());

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);

        //-------------------- getting pre stored username password from shared prefrences --------------------
        sp_uname = shared_prefs.getSharedPrefsUsername();
        sp_pwd = shared_prefs.getSharedPrefsPassword();
        if(sp_uname != null && sp_pwd != null){
            u_name.setText(sp_uname);
            passwd.setText(sp_pwd);
        }

        //-------------------- Handling clicks --------------------
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_out, R.anim.fade_in).replace(R.id.main_layout,new RegisterFragment()).commit();
            }
        });
        LOGIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //-------------------- Taking username password --------------------
                L_USERNAME = u_name.getText().toString();
                L_PASSWORD = passwd.getText().toString();
                //-------------------- Validating username password before sending to FIREBASE --------------------
                if(L_USERNAME.isEmpty()){
                    u_name.setError("Please enter a Username");
                    return;
                }
                if(L_PASSWORD.isEmpty()){
                    passwd.setError("Please enter Password");
                }
                if (L_PASSWORD.length() <= 5) {
                    passwd.setError("Password invalid: Length too short");
                    return;
                }

                shared_prefs.setSharedPrefs(L_USERNAME,L_PASSWORD);

                firebaseAuth.signInWithEmailAndPassword(L_USERNAME,L_PASSWORD).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    commonMethods.showDialog("","You have successfully logged in");
                                    Common_Methods.currentUser = firebaseAuth.getCurrentUser();
                                    Common_Methods.USER_ID = firebaseAuth.getCurrentUser().getUid();
                                    //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new ProfileFragment()).commit();
                                }else
                                    Toast.makeText(getActivity(), "Login Failed", Toast.LENGTH_SHORT).show();

                            }
                        });

            }
        });

        g_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        //------------Registration with facebook-------------
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        loginButton = v.findViewById(R.id.facebook_btn);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Log.d(TAG, "facebook:onSuccess:" + loginResult);
                Toast.makeText(getActivity(), "Successfully logged in with Facebook", Toast.LENGTH_SHORT).show();
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                //Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                //Log.d(TAG, "facebook:onError", error);
                Toast.makeText(getActivity(), "Facebook login failed", Toast.LENGTH_SHORT).show();
                // ...
            }
        });
        // ...


        return v;
    }


    //------------------facebook AUTH--------------------
    private void handleFacebookAccessToken(AccessToken token) {
        //Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            //Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getActivity(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }


    //------------------google auth----------------------
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
                Toast.makeText(getActivity(), "Google sign in successfull", Toast.LENGTH_SHORT).show();
                //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new ProfileFragment()).commit();
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                //Log.w(TAG, "Google sign in failed", e);
                Toast.makeText(getActivity(), "Google sign in failed", Toast.LENGTH_SHORT).show();
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        //Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            Common_Methods.currentUser = firebaseAuth.getCurrentUser();
                            Common_Methods.USER_ID = firebaseAuth.getCurrentUser().getUid();
                            Toast.makeText(getActivity(), "SignInWithCredential:Success", Toast.LENGTH_SHORT).show();
                            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_out, R.anim.fade_in).replace(R.id.main_layout,new ProfileFragment()).commit();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            //Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getActivity(), "signInWithCredential:failure", Toast.LENGTH_SHORT).show();
                            //Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }

}
