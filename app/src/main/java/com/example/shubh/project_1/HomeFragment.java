package com.example.shubh.project_1;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

import static android.content.Context.LOCATION_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    LocationManager lm;
    static ViewPager vp;
    int currentPage = 0;
    SharedPreferences sp1;
    SharedPreferences.Editor editor1;

    static final Integer[] images = Common_Methods.Common_images;
    ArrayList<Integer> picArray = new ArrayList<Integer>();
    int img_len,temp_loc;

    LocationManager locationManager;
    FirebaseAuth mAuth;
    FirebaseUser mUser;
    String  USERID;
    double longitude,lattitude,longitude1,lattitude1;
    TextView tv_loc,locate_car,tv,tv1;
    ImageView img_v;
    Button save_location,take_to_mycar;
    String longi,lati;
    Common_Methods commonMethods;
    DatabaseReference dataReference;


    Dialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        getActivity().setTitle("My Honda");
        save_location = v.findViewById(R.id.save_btn);
        take_to_mycar = v.findViewById(R.id.take_my_car);
        img_v = v.findViewById(R.id.sos);
        locate_car = v.findViewById(R.id.locating_car);
        tv = v.findViewById(R.id.car_location);
        tv1 = v.findViewById(R.id.car_onMap);

        //--------------------Shared Prefrences for location---------------------
        sp1 = getActivity().getSharedPreferences("MyHondaLocation", Context.MODE_PRIVATE);
        editor1 = sp1.edit();

        tv.setEnabled(false);
        tv1.setEnabled(false);
        commonMethods = new Common_Methods(getActivity());
        //--------------------Location Manager for location---------------------
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},0);
        }

        tv_loc = v.findViewById(R.id.tv_location);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                lattitude = location.getLatitude();
                longitude = location.getLongitude();
                //it is a class which converts longitude lattitude values into address.
                try {
                    Geocoder geocoder = new Geocoder(getActivity());
                    List<Address> list = geocoder.getFromLocation(lattitude, longitude, 1);
                    String country = list.get(0).getCountryName();
                    String locality = list.get(0).getLocality();
                    String postalcode = list.get(0).getPostalCode();
                    String adr = list.get(0).getAddressLine(0);
                    list.get(0).getAddressLine(0);
                    tv_loc.setText("\n\n"+country+", "+locality+", "+postalcode+", "+adr);
                } catch (Exception e) {

                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });


        for(int i=0; i<images.length; i++) {
            picArray.add(images[i]);
        }
            vp = v.findViewById(R.id.pager);
            vp.setAdapter(new MyViewPagerAdapter(getActivity(),picArray));

            CircleIndicator indicator = v.findViewById(R.id.indicator);
            indicator.setViewPager(vp);

            img_len = images.length;
            img_len++;
            final Handler handler = new Handler();
            final Runnable update = new Runnable() {
                @Override
                public void run() {
                    if(currentPage == img_len-1){
                        currentPage=0;
                    }
                    vp.setCurrentItem(currentPage++,true);
                }
            };

            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(update);
                }
            },3000,3000);



            //*************** saving current parking location ***************

        save_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(longitude == 0 && lattitude == 0){
                    String msg = "Please wait until your location is obtained...";
                    commonMethods.showDialog("",msg);
                    return;
                }
                else{
                    longi = String.valueOf(longitude);
                    lati = String.valueOf(lattitude);
                    editor1.clear();
                    editor1.putString("longitude",longi);
                    editor1.putString("lattitude",lati);
                    editor1.commit();
                    String msg = "Your Location is Saved";
                    commonMethods.showDialog("",msg);
                }
            }
        });

        take_to_mycar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                longitude = Double.parseDouble(sp1.getString("longitude",null));
                lattitude = Double.parseDouble(sp1.getString("lattitude",null));

                //commonMethods.showDialog("","Open google maps to locate your car");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        GoogleMapsMethos();
                    }
                }, 1500);

            }
        });

        img_v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(longitude == 0 & lattitude == 0) {
                    commonMethods.showDialog("Please Wait...","Trying to get your loacation");
                }
                else {
                    commonMethods.sendSOS(longitude, lattitude);
                }
            }
        });

        locate_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    try{
                        mAuth = FirebaseAuth.getInstance();
                        mUser = mAuth.getCurrentUser();
                        USERID = mUser.getUid();
                    }catch (Exception e){
                        commonMethods.showDialog("","You are not logged in");
                    }
                    dataReference = FirebaseDatabase.getInstance().getReference().child("users").child(USERID).child("location");
                    dataReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            try {
                                Map<Double, Object> map1 = (Map<Double, Object>) dataSnapshot.getValue();
                                lattitude1 = (Double) map1.get("Lattitude");
                                longitude1 = (Double) map1.get("Longitude");
                                ViewGroup.LayoutParams params = tv.getLayoutParams();
                                // Changes the height and width to the specified *pixels*
                                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                                tv.setText("Longitude : "+longitude1+"\nLattitude : "+lattitude1);
                                tv.setEnabled(true);
                                tv1.setEnabled(true);
                            }
                            catch (Exception e){
                                commonMethods.showDialog("CAR GPS Disabled","Unable to locate your car");
                            }

                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }

                    });
                }catch (Exception e){
                    commonMethods.showDialog("Sorry...","Unable to get location ");
                }
            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tv.isEnabled()){
                    /*
                    longitude = longitude1;
                    lattitude = lattitude1;
                    GoogleMapsMethos();
                    */
                    String strUri = "http://maps.google.com/maps?q=loc:" + lattitude1 + "," + longitude1 + " (" + "Here is your honda" + ")";
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);

                    tv.setEnabled(false);
                    tv.setText("");
                    tv1.setEnabled(false);
                }
            }
        });
        return v;
    }

    private void GoogleMapsMethos() {
        String label = "My Honda ";
        String uriBegin = "geo:" + lattitude + "," + longitude;
        String query = lattitude + "," +longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" +encodedQuery + "&z=16";
        Uri uri= Uri.parse(uriString);
        Intent mapIntent=new Intent(Intent.ACTION_VIEW,uri);
        startActivity(mapIntent);
    }


}
