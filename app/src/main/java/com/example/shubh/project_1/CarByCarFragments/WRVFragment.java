package com.example.shubh.project_1.CarByCarFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.shubh.project_1.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class WRVFragment extends Fragment {


    public WRVFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wrv, container, false);
    }

}
