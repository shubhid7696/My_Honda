package com.example.shubh.project_1.Extra_Classes;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;

public class Shared_Prefs {
    Activity activity;
    SharedPreferences sp ;
    SharedPreferences.Editor editor;

    String string;

    public Shared_Prefs(FragmentActivity activity) {
        this.activity = activity;
        sp = activity.getSharedPreferences("MyHondaFile", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public void setSharedPrefs(String l_username, String l_password) {

        editor.putString("user_name",l_username);
        editor.putString("user_passwd",l_password);
        editor.commit();
    }

    public void getSharedPrefs(){

    }

    public String getSharedPrefsUsername() {
        string = sp.getString("user_name",null);
        return string;
    }

    public String getSharedPrefsPassword() {
        string = sp.getString("user_passwd",null);
        return string;
    }

    public void clearSharedPrefs() {
        editor.clear();
        editor.commit();
    }
}
