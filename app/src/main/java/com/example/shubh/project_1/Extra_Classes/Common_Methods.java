package com.example.shubh.project_1.Extra_Classes;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shubh.project_1.Activities.MainActivity;
import com.example.shubh.project_1.Activities.WebViewActivity;
import com.example.shubh.project_1.R;
import com.google.firebase.auth.FirebaseUser;

public class  Common_Methods {

    //------------------- common entites used in project ------------------
    public static final Integer[] Common_images = {
            R.drawable.vp1,
            R.drawable.city_banner,
            R.drawable.vp2,
            R.drawable.vp3,
            R.drawable.vp5,
            R.drawable.vp6};
    public static String USER_ID = null;
    public static FirebaseUser currentUser;


    public static String FragmentName = null;
    Activity activity;
    Dialog dialog;

    public Common_Methods(Activity activity) {
        this.activity=activity;
        dialog = new Dialog(activity);
    }



    //****************************Make a call Dialog****************************
    public void Make_Call(){
        final String str_number = activity.getResources().getString(R.string.customer_care_number);
        final String no_action = activity.getResources().getString(R.string.free_to_call);
        dialog.setContentView(R.layout.custom_dialog_call);
        Button btn_yes = dialog.findViewById(R.id.yes);
        Button btn_no = dialog.findViewById(R.id.no);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_CALL);
                i.setData(Uri.parse("tel:"+str_number));
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CALL_PHONE},0);
                    return;
                }
                activity.startActivity(i);
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, no_action, Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    //useless comment

   public void OpenWebsite() {
        final String india_website = activity.getResources().getString(R.string.india_site);
        dialog.setContentView(R.layout.custom_dialog_website);
       Button btn_yes = dialog.findViewById(R.id.mail_yes);
       Button btn_no = dialog.findViewById(R.id.mail_no);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity,WebViewActivity.class);
                i.putExtra("website_name",india_website);
                activity.startActivity(i);
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
   }

    public void Send_Mail() {
        dialog.setContentView(R.layout.custom_dialog_mail);
        Button open_mailTo = dialog.findViewById(R.id.mail_layout);
        open_mailTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog3 = new Dialog(activity);
                dialog3.setContentView(R.layout.custom_dialog_send_mail);
                final EditText mail_subject = dialog3.findViewById(R.id.mail_subject);
                final EditText mail_message = dialog3.findViewById(R.id.mail_message);
                Button send_mail = dialog3.findViewById(R.id.send_mail_to);

                send_mail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String Subject,Message,mailTo;

                        Subject = mail_subject.getText().toString();
                        Message = mail_message.getText().toString();
                        mailTo = activity.getResources().getString(R.string.to_mail);

                        String to[] = {mailTo};

                        Intent send_mail_intent = new Intent(Intent.ACTION_SEND);
                        send_mail_intent.setData(Uri.parse("to:"));

                        send_mail_intent.putExtra(Intent.EXTRA_EMAIL,to);
                        send_mail_intent.putExtra(Intent.EXTRA_SUBJECT,Subject);
                        send_mail_intent.putExtra(Intent.EXTRA_TEXT,Message);

                        send_mail_intent.setType("message/utf-8");
                        activity.startActivity(Intent.createChooser(send_mail_intent,"Email"));
                        dialog3.dismiss();
                    }
                });
                dialog3.show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void sendSOS(double longitude, double lattitude) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.SEND_SMS},0);
            return;
        }
        dialog.setContentView(R.layout.send_sms);
        Button send = dialog.findViewById(R.id.se_nd);
        final EditText infor = dialog.findViewById(R.id.info);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = "Need Urgent Help "+infor.getText().toString();
                SmsManager message = SmsManager.getDefault();
                String phone = "7696686988";
                message.sendTextMessage(phone,null,str,null,null);
                Toast.makeText(activity, "Message sent...", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showDialog(String title,String msg){
        dialog.setContentView(R.layout.common_dialog);
        Button ok = dialog.findViewById(R.id.dialog_btn);
        if (title.isEmpty())
            title = activity.getResources().getString(R.string.my_honda);
        if(msg.isEmpty())
            msg = "Unable to get message";

        TextView tv1 = dialog.findViewById(R.id.dialog_title);
        TextView tv2 = dialog.findViewById(R.id.dialog_mesg);

        tv1.setText(title);
        tv2.setText(msg);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}
