package com.example.shubh.project_1.CarByCarFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.shubh.project_1.MyViewPagerAdapter;
import com.example.shubh.project_1.R;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;


/**
 * A simple {@link Fragment} subclass.
 */
public class BrioFragment extends Fragment implements View.OnClickListener {

    ImageView iv;
    Button b1,b2,b3,b4,b5;
    ViewPager vp;
    Integer brio_pics[] = {R.drawable.bi1,R.drawable.bi2,R.drawable.bi3,R.drawable.bi4,R.drawable.bi5,R.drawable.bi6,
            R.drawable.bi7,R.drawable.bi8,R.drawable.bi9,R.drawable.bi10,R.drawable.bi11,R.drawable.bi12,R.drawable.bi13};

    ArrayList<Integer> picArray = new ArrayList<Integer>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_brio, container, false);
        iv = v.findViewById(R.id.brio_color);

        b1 = v.findViewById(R.id.r_red);
        b2 = v.findViewById(R.id.a_silver);
        b3 = v.findViewById(R.id.m_silver);
        b4 = v.findViewById(R.id.o_pearl);
        b5 = v.findViewById(R.id.t_white);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);


        vp = v.findViewById(R.id.pager);
        for(int i=0; i<brio_pics.length; i++) {
            picArray.add(brio_pics[i]);
        }

        vp.setAdapter(new MyViewPagerAdapter(getActivity(),picArray));

        CircleIndicator indicator = v.findViewById(R.id.indicator);
        indicator.setViewPager(vp);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.r_red:
                iv.setImageResource(R.drawable.rallye_red_brio);
                break;
            case R.id.a_silver:
                iv.setImageResource(R.drawable.alabaster_silver_brio);
                break;
            case R.id.m_silver:
                iv.setImageResource(R.drawable.urban_titanium_brio);
                break;
            case R.id.o_pearl:
                iv.setImageResource(R.drawable.orchid_pearl_brio);
                break;
            case R.id.t_white:
                iv.setImageResource(R.drawable.taffeta_white_brio);
                break;
        }
    }
}
