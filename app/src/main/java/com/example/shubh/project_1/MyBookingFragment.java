package com.example.shubh.project_1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.example.shubh.project_1.User_Fragments.LogIn;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyBookingFragment extends Fragment {

    View v1,v2;
    TextView btv,btv1,btv2,btv3,tv4,tv5,tv6,tv7,tv8,b_title;
    String date,time,engine,model,payment;
    String date1,time1,engine1,model1,payment1,phone1,address;
    LinearLayout linearLayout1,linearLayout2;
    FirebaseAuth mAuth;
    FirebaseUser current_user;
    String userID;
    Common_Methods cm;
    DatabaseReference firebaseDatabase,bookingRefrence;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_booking, container, false);

        getActivity().setTitle("Bookings");
        cm = new Common_Methods(getActivity());
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        if(mAuth == null)
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new LogIn()).commit();
        else{
            current_user = mAuth.getCurrentUser();
            if (current_user == null){}
            else
                userID = mAuth.getCurrentUser().getUid();
        }

        if(firebaseDatabase == null){
            Toast.makeText(getActivity(), "No Connection to database", Toast.LENGTH_SHORT).show();
        }
        else {
            getFromBookings();
            getFromTests();
        }

        //===========================getting id's===========================
        btv = v.findViewById(R.id.booking_model);
        btv1 = v.findViewById(R.id.booking_engine);
        btv2 = v.findViewById(R.id.booking_date_time);
        btv3 = v.findViewById(R.id.booking_payment_method);
        tv4 = v.findViewById(R.id.testing_model);
        tv5 = v.findViewById(R.id.testing_engine);
        tv6 = v.findViewById(R.id.testing_date_time);
        tv7 = v.findViewById(R.id.testing_phone);
        tv8 = v.findViewById(R.id.testing_address);
        b_title = v.findViewById(R.id.bookings_title);

        linearLayout1 = v.findViewById(R.id.booking_layout);
        linearLayout2 = v.findViewById(R.id.testing_layout);
        v1 = v.findViewById(R.id.testing_View1);
        v2 = v.findViewById(R.id.testing_View2);


        return v;
    }


    public void getFromBookings(){
        try {
            bookingRefrence = firebaseDatabase.child("bookings").child(userID);
        }catch (Exception e){

        }
        if(bookingRefrence == null) {
            Toast.makeText(getActivity(), "No Connection to database", Toast.LENGTH_SHORT).show();
        }else{

            bookingRefrence.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                        if (map.isEmpty()) {
                            Toast.makeText(getActivity(), "No data to show", Toast.LENGTH_SHORT).show();
                        } else {
                            time = (String) map.get("Time");
                            date = (String) map.get("Date");
                            engine = (String) map.get("Engine");
                            model = (String) map.get("Model");
                            payment = (String) map.get("Payment Method");


                            btv.append(model);
                            btv1.append(engine);
                            btv2.append(date+" "+time);
                            btv3.append(payment);

                            ViewGroup.LayoutParams params = linearLayout1.getLayoutParams();
                            // Changes the height and width to the specified *pixels*
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                            linearLayout1.setLayoutParams(params);

                            v1.setVisibility(View.VISIBLE);
                            b_title.setVisibility(View.INVISIBLE);
                            linearLayout1.setPadding(18,2,18,2);
                        }
                    }catch (Exception e){
                        Toast.makeText(getActivity(), "Nothing to show", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    public void getFromTests(){
        try {
            bookingRefrence = firebaseDatabase.child("testing").child(userID);
        }catch (Exception e){

        }
        if(bookingRefrence == null) {
            Toast.makeText(getActivity(), "No Connection to database", Toast.LENGTH_SHORT).show();
        }else{

            bookingRefrence.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                        if (map.isEmpty()) {
                            Toast.makeText(getActivity(), "No data to show", Toast.LENGTH_SHORT).show();
                        } else {
                            time1 = (String) map.get("Time");
                            date1 = (String) map.get("Date");
                            engine1 = (String) map.get("Engine");
                            phone1 = (String) map.get("Phone");
                            model1 = (String) map.get("Model");
                            address = (String) map.get("Address");

                            tv4.append(model1);
                            tv5.append(engine1);
                            tv6.append(date+" "+time);
                            tv7.append(phone1);
                            tv8.append(address);

                            ViewGroup.LayoutParams params = linearLayout2.getLayoutParams();
                            // Changes the height and width to the specified *pixels*
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                            linearLayout2.setLayoutParams(params);
                            linearLayout2.setPadding(18,2,18,2);

                            v2.setVisibility(View.VISIBLE);
                            b_title.setVisibility(View.INVISIBLE);
                        }
                    }catch (Exception e){
                        Toast.makeText(getActivity(), "Nothing to show", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

}




