package com.example.shubh.project_1.User_Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.example.shubh.project_1.Extra_Classes.FirebaseMethods;
import com.example.shubh.project_1.R;
import com.example.shubh.project_1.Extra_Classes.Shared_Prefs;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    int id;
    Shared_Prefs shared_pref;
    Button logout,edit;
    EditText Euser_name,Euser_phone,Euser_address,Euser_age,Euser_drl;
    String name,phone,address,age,drl;
    FirebaseUser firebaseUser;
    FirebaseAuth firebaseAuth1;
    DatabaseReference databaseReference1;
    FirebaseMethods firebaseMethods;
    Common_Methods commonMethods;
    String user_id;

    @Override
    public void onStart() {
        firebaseMethods = new FirebaseMethods(getActivity());
        commonMethods = new Common_Methods(getActivity());
        firebaseAuth1 = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth1.getCurrentUser();
        firebaseMethods.checkLogin();
        if(firebaseUser==null){
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new LogIn()).commit();
        }
        else{
            Common_Methods.currentUser = firebaseUser;
            Common_Methods.USER_ID = firebaseUser.getUid();
            user_id = firebaseUser.getUid();
            databaseReference1=FirebaseDatabase.getInstance().getReference();
        }

        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        getActivity().setTitle("Profile");

        onStart();


        if (databaseReference1==null)
            commonMethods.showDialog("","No connection to Database\nor\nNo Data in database");
        else
            getDataFromDatabase();

        shared_pref = new Shared_Prefs(getActivity());
        logout = v.findViewById(R.id.logout);
        Euser_name = v.findViewById(R.id.user_name);
        Euser_phone = v.findViewById(R.id.user_phone);
        Euser_address = v.findViewById(R.id.user_address);
        Euser_age = v.findViewById(R.id.user_age);
        edit = v.findViewById(R.id.edit_profile_btn);
        Euser_drl = v.findViewById(R.id.user_drivLis);

        editTextsfalse();

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit.getText().toString() == "edit profile") {
                    edit.setText("save data");
                    editTextstrue();
                }else{
                    edit.setText("edit profile");
                    editTextsfalse();
                    saveDataToDatabase();
                }
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common_Methods.USER_ID=null;
                    firebaseAuth1.signOut();

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout, new LogIn()).commit();
            }
        });
        return v;
    }

    private void editTextstrue() {
        Euser_name.setEnabled(true);
        Euser_phone.setEnabled(true);
        Euser_address.setEnabled(true);
        Euser_age.setEnabled(true);
        Euser_drl.setEnabled(true);
    }

    private void editTextsfalse() {
        Euser_name.setEnabled(false);
        Euser_phone.setEnabled(false);
        Euser_address.setEnabled(false);
        Euser_age.setEnabled(false);
        Euser_drl.setEnabled(false);
    }

    private void saveDataToDatabase() {
        name = Euser_name.getText().toString().trim();
        phone = Euser_phone.getText().toString().trim();
        address = Euser_address.getText().toString();
        age = Euser_age.getText().toString();
        drl = Euser_drl.getText().toString();
        //-------------------------validating data
        if(name.isEmpty() || name.length()<1) {
            Euser_name.setError("Please enter a valid Name");
            return;
        }
        if(phone.isEmpty() || phone.length()<10 || phone.length()>10) {
            Euser_phone.setError("Please enter a valid Phone");
            return;
        }
        if(address.isEmpty()) {
            Euser_address.setError("Please enter a valid Address");
            return;
        }
        if(age.isEmpty() ) {
            Euser_age.setError("Please enter a valid Age");
            return;
        }
        if(drl.isEmpty() || drl.length()<5) {
            Euser_drl.setError("Please enter a valid Licence number");
            return;
        }

        Map newPost = new HashMap();
        newPost.put("Name",name);
        newPost.put("Phone",phone);
        newPost.put("Age",age);
        newPost.put("Address",address);
        newPost.put("Licence",drl);
        newPost.put("Car_id","");
        newPost.put("Booking_id","");
        newPost.put("Testing_id","");
        newPost.put("Service_id","");

        DatabaseReference current_user_database = databaseReference1.child("users").child(user_id);
        current_user_database.setValue(newPost);
    }

    private void getDataFromDatabase(){
        DatabaseReference curentuserDB = databaseReference1.child("users").child(user_id);
        curentuserDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if (map.isEmpty()) {
                        Toast.makeText(getActivity(), "No data to show", Toast.LENGTH_SHORT).show();
                    } else {
                        name = (String) map.get("Name");
                        age = (String) map.get("Age");
                        phone = (String) map.get("Phone");
                        address = (String) map.get("Address");
                        drl = (String) map.get("Licence");
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), "Nothing to show", Toast.LENGTH_SHORT).show();
                }

                    Euser_name.setText(name);
                    Euser_phone.setText(phone);
                    Euser_address.setText(address);
                    Euser_age.setText(age);
                    Euser_drl.setText(drl);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
