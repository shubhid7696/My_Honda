package com.example.shubh.project_1.Extra_Classes;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v4.app.FragmentActivity;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class Date_Time {

    Activity activity;
    Calendar ca = Calendar.getInstance();
    String string,str1;
    int id;

    public Date_Time(Activity activity) {
        this.activity=activity;
    }

    //-------------------- getting date  --------------------
    public String getDate(){

        int day,month,year;
        day = ca.get(Calendar.DAY_OF_MONTH);
        month = ca.get(Calendar.MONTH);
        month++;
        year = ca.get(Calendar.YEAR);

        String str = "Date: "+day+"/"+month+"/"+year;

        return str;
    }

    //-------------------- getting time  --------------------
    public String getTime() {
        String dur;
        int hour,min;
        hour = ca.get(Calendar.HOUR_OF_DAY);
        min = ca.get(Calendar.MINUTE);
        if(hour>12){
            hour-=12;
            dur = "PM";
        }
        else
            dur = "AM";

        String str = "Time: "+hour+":"+min+" "+dur;

        return str;
    }

    public void getSelctedDate(FragmentActivity activity, int id) {
        this.id = id;
        this.activity = activity;
        final TextView tv = activity.findViewById(id);
        new DatePickerDialog(this.activity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                str1 = "Date: "+dayOfMonth+"/"+month+"/"+year;
                tv.setText(str1);
            }
        }, ca.get(Calendar.DAY_OF_MONTH), ca.get(Calendar.MONTH), ca.get(Calendar.YEAR)).show();

    }

    public void getSelectedTime(FragmentActivity activity, int id) {
        this.id = id;
        this.activity = activity;
        final TextView tv = activity.findViewById(id);
        new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                str1 = "Time: "+hourOfDay+":"+minute;
                tv.setText(str1);
            }
        }, ca.get(Calendar.HOUR_OF_DAY), ca.get(Calendar.MINUTE), false).show();
    }
}
