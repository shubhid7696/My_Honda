package com.example.shubh.project_1.Car_Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.example.shubh.project_1.Extra_Classes.Date_Time;
import com.example.shubh.project_1.R;
import com.example.shubh.project_1.User_Fragments.LogIn;
import com.example.shubh.project_1.User_Fragments.ProfileFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Vehicle_Entry_Fragment extends Fragment {

    FirebaseAuth firebaseAuth2;
    FirebaseUser currentUser;
    DatabaseReference databaseReference;
    DatabaseReference curentuserDB;
    String user_id;
    String car_name,phone,model,owner_name,chassie,engine_no,year,color,fuel_type,spin_model;
    EditText Ecar_name,Ephone,Emodel,PownName,PChassie,EngineNo,Eyear,Ecolor;
    Spinner spin;
    RadioButton pet,dis;
    String str[];
    Button edit_car;


    @Override
    public void onStart() {

        FirebaseUser currentUser = firebaseAuth2.getCurrentUser();
        if(currentUser ==null)
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new LogIn()).commit();
        else{

        }
        super.onStart();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_vehicle__entry_, container, false);

        getActivity().setTitle("My Car");
        firebaseAuth2 = FirebaseAuth.getInstance();
        if(firebaseAuth2 == null){
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new LogIn()).commit();
        }
        else {
            currentUser = firebaseAuth2.getCurrentUser();
            if (currentUser == null) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout, new LogIn()).commit();
            } else {
                user_id = firebaseAuth2.getCurrentUser().getUid();
                databaseReference = FirebaseDatabase.getInstance().getReference();
                curentuserDB = databaseReference.child("users").child(user_id);
            }
        }

        Ecar_name = v.findViewById(R.id.p_car_name);
        Ephone = v.findViewById(R.id.p_phone);
        spin = v.findViewById(R.id.p_spinner_list);
        Emodel = v.findViewById(R.id.p_model);
        PownName = v.findViewById(R.id.p_owner_name);
        PChassie = v.findViewById(R.id.p_chassie);
        EngineNo = v.findViewById(R.id.Engine_no);
        Eyear = v.findViewById(R.id.p_year);
        pet = v.findViewById(R.id.petrol);
        dis = v.findViewById(R.id.disel);
        Ecolor = v.findViewById(R.id.p_color);
        edit_car = v.findViewById(R.id.save_car_data);


        disableAllEntities();

        getDataFromDatabase();//-----------get data from database---------------
        str = getResources().getStringArray(R.array.car_list_array);
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,str);

        spin.setAdapter(adp);

        spin_model = "No Selected Car";
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spin_model = adp.getItem(position).toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        edit_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pet.isChecked())
                    fuel_type = "petrol";
                if(dis.isChecked())
                    fuel_type="disel";

                if(edit_car.getText().toString() == "Edit Details"){
                    edit_car.setText("save data");
                    enableAllEntities();
                }
                else{
                    edit_car.setText("Edit Details");
                    disableAllEntities();
                    saveDataToDatabase();
                }
            }
        });

        return v;
    }
    public void disableAllEntities(){
        Ecar_name.setEnabled(false);
        Ephone.setEnabled(false);
        Emodel.setEnabled(false);
        PownName.setEnabled(false);
        PChassie.setEnabled(false);
        EngineNo.setEnabled(false);
        Eyear.setEnabled(false);
        Ecolor.setEnabled(false);
        spin.setEnabled(false);
        pet.setEnabled(false);
        dis.setEnabled(false);

    }
    public void enableAllEntities(){
        Ecar_name.setEnabled(true);
        Ephone.setEnabled(true);
        Emodel.setEnabled(true);
        PownName.setEnabled(true);
        PChassie.setEnabled(true);
        EngineNo.setEnabled(true);
        Eyear.setEnabled(true);
        Ecolor.setEnabled(true);
        spin.setEnabled(true);
        pet.setEnabled(true);
        dis.setEnabled(true);
    }

    private void saveDataToDatabase() {
        car_name = Ecar_name.getText().toString().trim();
        phone = Ephone.getText().toString().trim();
        model = Emodel.getText().toString().trim();
        owner_name = PownName.getText().toString().trim();
        chassie = PChassie.getText().toString().trim();
        engine_no = EngineNo.getText().toString().trim();
        year = Eyear.getText().toString().trim();
        color = Ecolor.getText().toString().trim();

        if(year.length()>4 || year.length()<4){
            Eyear.setError("Please enter a valid year");
        }
        Map newPost = new HashMap();
        newPost.put("Car name",car_name);
        newPost.put("Phone",phone);
        newPost.put("Model",model);
        newPost.put("Car_Model",spin_model);
        newPost.put("Owner",owner_name);
        newPost.put("Chassie",chassie);
        newPost.put("Engine",engine_no);
        newPost.put("Year",year);
        newPost.put("Color",color);
        newPost.put("Fuel_Type",fuel_type);
        DatabaseReference currentcarDB = curentuserDB.child("car");
        currentcarDB.setValue(newPost);

    }

    private void getDataFromDatabase(){
        if (curentuserDB == null)
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new LogIn()).commit();
        else {
            curentuserDB.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Map<String, Object> map_user = (Map<String, Object>) dataSnapshot.getValue();
                    phone = (String) map_user.get("Phone");
                    String name = (String) map_user.get("Name");
                    String addr = (String) map_user.get("Address");

                    if (name.isEmpty() && phone.isEmpty())
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout, new ProfileFragment()).commit();
                    else {
                        DatabaseReference currentcarDB = curentuserDB.child("car");
                        currentcarDB.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                try {
                                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                                    fuel_type = (String) map.get("Fuel_Type");
                                    car_name = (String) map.get("Car name");
                                    model = (String) map.get("Model");
                                    owner_name = (String) map.get("Address");
                                    chassie = (String) map.get("Chassie");
                                    engine_no = (String) map.get("Engine");
                                    year = (String) map.get("Address");
                                    chassie = (String) map.get("Chassie");
                                    fuel_type = (String) map.get("Fuel_Type");
                                }catch (Exception e){
                                    Toast.makeText(getActivity(), "No Data to show", Toast.LENGTH_SHORT).show();
                                }
                                if (fuel_type == "petrol")
                                    pet.setChecked(true);
                                else
                                    dis.setChecked(true);

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                    Ecar_name.setText(car_name);
                    Ephone.setText(phone);
                    Emodel.setText(model);
                    PownName.setText(owner_name);
                    PChassie.setText(chassie);
                    EngineNo.setText(engine_no);
                    Eyear.setText(year);
                    Ecolor.setText(color);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

}
