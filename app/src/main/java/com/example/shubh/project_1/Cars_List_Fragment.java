package com.example.shubh.project_1;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.shubh.project_1.CarByCarFragments.AccordFragment;
import com.example.shubh.project_1.CarByCarFragments.AmazeFragment;
import com.example.shubh.project_1.CarByCarFragments.BRVFragment;
import com.example.shubh.project_1.CarByCarFragments.BrioFragment;
import com.example.shubh.project_1.CarByCarFragments.CRVFragment;
import com.example.shubh.project_1.CarByCarFragments.CityFragment;
import com.example.shubh.project_1.CarByCarFragments.JazzFragment;
import com.example.shubh.project_1.CarByCarFragments.WRVFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class Cars_List_Fragment extends Fragment {

    ListView l;
    Integer pics[] = {R.drawable.brio0,R.drawable.amaze0,R.drawable.jazz0,
            R.drawable.wrv0,R.drawable.city0,R.drawable.brv0,R.drawable.crv0,R.drawable.accord0};
    String names[];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cars__list_, container, false);
        names = getActivity().getResources().getStringArray(R.array.car_list_array);
        l = v.findViewById(R.id.car_list);

        CarsListAdapter cla = new CarsListAdapter(getActivity(),names,pics);
        l.setAdapter(cla);

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openCarFregment(position);
            }
        });

        return v;
    }

    private void openCarFregment(int position) {
        int pos = position;
        pos++;
        switch(pos){
            case 1:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new BrioFragment()).commit();
                break;
            case 2:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new AmazeFragment()).commit();
                break;
            case 3:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new JazzFragment()).commit();
                break;
            case 4:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new WRVFragment()).commit();
                break;
            case 5:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new CityFragment()).commit();
                break;
            case 6:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new BRVFragment()).commit();
                break;
            case 7:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new CRVFragment()).commit();
                break;
            case 8:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new AccordFragment()).commit();
                break;

        }
    }

    private class CarsListAdapter extends ArrayAdapter<String> {
        Activity activity;
        String car_name[];
        Integer pics[];

        public CarsListAdapter(FragmentActivity activity, String[] names, Integer[] pics) {
            super(activity,R.layout.cars_list,names);
            this.activity = activity;
            car_name = names;
            this.pics = pics;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = activity.getLayoutInflater();
            View v = inflater.inflate(R.layout.cars_list,null);
            TextView tv = v.findViewById(R.id.textView);
            ImageView iv = v.findViewById(R.id.imageView);

            tv.setText(car_name[position]);
            iv.setImageResource(pics[position]);
            return v;

        }
    }

}
