package com.example.shubh.project_1.Activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.example.shubh.project_1.Car_Fragments.AboutFragment;
import com.example.shubh.project_1.Car_Fragments.BookingFragment;
import com.example.shubh.project_1.Car_Fragments.TestFragment;
import com.example.shubh.project_1.Car_Fragments.Vehicle_Entry_Fragment;
import com.example.shubh.project_1.Cars_List_Fragment;
import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.example.shubh.project_1.HomeFragment;
import com.example.shubh.project_1.MyBookingFragment;
import com.example.shubh.project_1.PermissionCheck;
import com.example.shubh.project_1.R;
import com.example.shubh.project_1.User_Fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Common_Methods common_methods;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //passing current main activity refrence
        common_methods = new Common_Methods(MainActivity.this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CALL_PHONE,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},0);
            return;
        }

        //-------------------- Calling permission check classs --------------------
        PermissionCheck permissionCheck = new PermissionCheck(this);
        permissionCheck.location_check();

        getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new HomeFragment()).addToBackStack("frag_stack").commit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //webview activity.
                //startActivity(new Intent(MainActivity.this, WebViewActivity.class));
                View fabview = getLayoutInflater().inflate(R.layout.customer_support, null);


                //***********************  action to be performed on snackbar item clicks  ***********************
                Snackbar snackbar_customer = Snackbar.make(view,"Customer Support",3000).setAction("Action",null);
                ImageView call = fabview.findViewById(R.id.action_call);
                ImageView mail = fabview.findViewById(R.id.action_mail);
                ImageView website = fabview.findViewById(R.id.action_webpage);
                ImageView imv = fabview.findViewById(R.id.action_chat);
                call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        common_methods.Make_Call();
                    }
                });
                mail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        common_methods.Send_Mail();
                    }
                });
                website.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        common_methods.OpenWebsite();
                    }
                });
                imv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        common_methods.showDialog("We are sorry","This feature is available only for the prime customers");
                    }
                });
                Snackbar.SnackbarLayout layout_snackbar = (Snackbar.SnackbarLayout)snackbar_customer.getView();
                layout_snackbar.setPadding(0,0,0,0);//.setPadding method removes padding +space from the default theme
                layout_snackbar.addView(fabview);
                snackbar_customer.show();

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.my_profile) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new ProfileFragment()).commit();
        }
        else if (id == R.id.my_car){
            getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new Vehicle_Entry_Fragment()).commit();
        }        else if (id == R.id.my_bookings){
            getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new MyBookingFragment()).commit();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new HomeFragment()).commit();
        } else if (id == R.id.book_car){
            getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new BookingFragment()).commit();
        } else if (id == R.id.test_drive){
            getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new TestFragment()).commit();
        } else if(id == R.id.car_list) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new Cars_List_Fragment()).commit();
        }else if(id == R.id.about) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new AboutFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
