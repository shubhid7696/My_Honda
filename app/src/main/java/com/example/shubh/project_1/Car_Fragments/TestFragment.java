package com.example.shubh.project_1.Car_Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.example.shubh.project_1.Extra_Classes.Date_Time;
import com.example.shubh.project_1.Extra_Classes.FirebaseMethods;
import com.example.shubh.project_1.MyViewPagerAdapter;
import com.example.shubh.project_1.R;
import com.example.shubh.project_1.User_Fragments.LogIn;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;


/**
 * A simple {@link Fragment} subclass.
 */
public class TestFragment extends Fragment implements View.OnClickListener {


    static ViewPager vp;
    static final Integer[] images = Common_Methods.Common_images;
    private ArrayList<Integer> picArray = new ArrayList<Integer>();
    private String model_name,engine_type,date,time,adr,phone,user_ID;
    private RadioButton r1,r2;
    private String str[];
    private Spinner spinner,pay_meth;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;
    private Button submit;
    Common_Methods cm;
    EditText cte1,cte2;
    FirebaseMethods firebaseMethods;
    int id;
    TextView Text_date,Text_time;
    Date_Time dt;
    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final Calendar ca = Calendar.getInstance();
        View v = inflater.inflate(R.layout.fragment_test, container, false);

        activity = getActivity();
        getActivity().setTitle("Test Drive");
        cm = new Common_Methods(activity);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        if(firebaseUser == null)
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new LogIn()).commit();
        else
            user_ID = firebaseUser.getUid();


        for(int i=0; i<images.length; i++) {
            picArray.add(images[i]);
        }
        vp = v.findViewById(R.id.test_pager);
        vp.setAdapter(new MyViewPagerAdapter(getActivity(),picArray));
        CircleIndicator indicator = v.findViewById(R.id.indicator);
        indicator.setViewPager(vp);


        spinner = v.findViewById(R.id.car_list2);
        Text_date = v.findViewById(R.id.date);
        Text_time = v.findViewById(R.id.time);
        r1 = v.findViewById(R.id.petrol);
        r2 = v.findViewById(R.id.disel);
        cte1 = v.findViewById(R.id.Test_adr);
        cte2 = v.findViewById(R.id.Test_phone);
        submit = v.findViewById(R.id.test_submit);

        //--------------creating class objects----------------
        firebaseMethods = new FirebaseMethods(activity);
        dt = new Date_Time(getActivity());

        String str_date = dt.getDate();//-------------------- Getting date --------------------
        String str_time = dt.getTime();//-------------------- Getting time --------------------

        Text_date.setText(str_date);//-------------------- Setting date --------------------
        Text_time.setText(str_time);//-------------------- Setting time --------------------

        str = getResources().getStringArray(R.array.car_list_array);
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,str);
        spinner.setAdapter(adp);

        model_name = "not selected";
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                model_name = adp.getItem(position).toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //-------------------- getting date. --------------------
        Text_date.setOnClickListener(this);
        //-------------------- getting time. --------------------
        Text_time.setOnClickListener(this);
        submit.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.date:
                id = Text_date.getId();
                dt.getSelctedDate(getActivity(),id);
                break;
            case R.id.time:
                id = Text_time.getId();
                dt.getSelectedTime(getActivity(),id);
                break;
            case R.id.test_submit:
                adr = cte1.getText().toString();
                phone = cte2.getText().toString();

                if (adr.isEmpty()){
                    //cte1.setError("Please enter your valid address");
                    Toast.makeText(getActivity(), "Please enter your valid address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (phone.isEmpty()){
                    cte2.setError("Please enter your valid phone number");
                    return;
                }

                if (r1.isChecked())
                    engine_type = "Petrol";
                else
                    engine_type = "Disel";

                date = Text_date.getText().toString().trim();
                time = Text_time.getText().toString().trim();

                Map newPost = new HashMap();
                newPost.put("Date",date);
                newPost.put("Time",time);
                newPost.put("Engine",engine_type);
                newPost.put("Model",model_name);
                newPost.put("Address",adr);
                newPost.put("Phone",phone);


                firebaseMethods.setTsestDataOnFirebase(newPost);

                cm.showDialog("BOOK TEST DRIVE","Your request has been submitted our agent will contact you soon");

                break;

        }
    }
}
