package com.example.shubh.project_1.Car_Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.shubh.project_1.Extra_Classes.Common_Methods;
import com.example.shubh.project_1.Extra_Classes.Date_Time;
import com.example.shubh.project_1.Extra_Classes.FirebaseMethods;
import com.example.shubh.project_1.MyViewPagerAdapter;
import com.example.shubh.project_1.R;
import com.example.shubh.project_1.User_Fragments.LogIn;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;


/**
 * A simple {@link Fragment} subclass.
 */
public class BookingFragment extends Fragment implements View.OnClickListener {

    static ViewPager vp;
    static final Integer[] images = Common_Methods.Common_images;
    private ArrayList<Integer> picArray = new ArrayList<Integer>();
    private TextView Text_date,Text_time;
    private String car_name,model_name,engine_type,date,time,payment_method,user_ID;
    private RadioButton r1,r2;
    private String str[];
    private String str2[] = {"CASH","CHEQUE","EMI","Other"};
    private Spinner Model_spinner,pay_meth;
    private Button submit;
    private Date_Time dt;
    private Activity activity;
    private int id;
    Common_Methods cm;
    FirebaseMethods firebaseMethods;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_booking, container, false);
        final Calendar ca = Calendar.getInstance();
        activity = getActivity();

        getActivity().setTitle("Book a Car");
        cm = new Common_Methods(activity);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        if(firebaseUser == null)
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new LogIn()).commit();
        else
            user_ID = firebaseUser.getUid();

        for(int i=0; i<images.length; i++) {
            picArray.add(images[i]);
        }
        vp = v.findViewById(R.id.booking_pager);
        vp.setAdapter(new MyViewPagerAdapter(getActivity(),picArray));

        CircleIndicator indicator = v.findViewById(R.id.booking_indicator);
        indicator.setViewPager(vp);


        //--------------creating class objects----------------
        firebaseMethods = new FirebaseMethods(activity);
        dt = new Date_Time(activity);

        Text_date = v.findViewById(R.id.booking_date);
        Text_time = v.findViewById(R.id.booking_time);
        submit = v.findViewById(R.id.booking_submit);
        Model_spinner = v.findViewById(R.id.car_list);
        pay_meth = v.findViewById(R.id.payment_method);
        r1 = v.findViewById(R.id.petrol);
        r2 = v.findViewById(R.id.disel);

        str = getResources().getStringArray(R.array.car_list_array);
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,str);
        final ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,str2);
        Model_spinner.setAdapter(adp);
        pay_meth.setAdapter(adp2);

        model_name = "Not Decided";
        payment_method = "other";
        Model_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                model_name = adp.getItem(position).toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        pay_meth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                payment_method = adp2.getItem(position).toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        String str_date = dt.getDate();//-------------------- Getting date --------------------
        String str_time = dt.getTime();//-------------------- Getting time --------------------

        Text_date.setText(str_date);//-------------------- Setting date --------------------
        Text_time.setText(str_time);//-------------------- Setting time --------------------

        Text_date.setOnClickListener(this);
        Text_time.setOnClickListener(this);

        submit.setOnClickListener(this);


        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.booking_date:
                id = Text_date.getId();
                dt.getSelctedDate(getActivity(),id);
                break;
            case R.id.booking_time:
                id = Text_time.getId();
                dt.getSelectedTime(getActivity(),id);
                break;
            case R.id.booking_submit:
                if (Common_Methods.USER_ID==null){
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_layout,new LogIn()).commit();
                }
                else {
                    Map newPost = new HashMap();
                    if (r1.isChecked())
                        engine_type = "Petrol";
                    else
                        engine_type = "Disel";

                    date = Text_date.getText().toString().trim();
                    time = Text_time.getText().toString().trim();
                    newPost.put("Date", date);
                    newPost.put("Time", time);
                    newPost.put("Engine", engine_type);
                    newPost.put("Model", model_name);
                    newPost.put("Payment Method", payment_method);
                    firebaseMethods.setBookingDataOnFirebase(newPost);

                    cm.showDialog("BOOK A CAR","Your request has been submitted our agent will contact you soon");
                }
                break;
        }
    }
}
